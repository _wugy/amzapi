package amzapi

// Region constants
type Region string

const (
	// RegionBrazil Brazil
	RegionBrazil Region = "BR"
	// RegionCanada Canada
	RegionCanada Region = "CA"
	// RegionChina  China
	RegionChina Region = "CN"
	// RegionGermany Germany
	RegionGermany Region = "DE"
	// RegionSpain  Spain
	RegionSpain Region = "ES"
	// RegionFrance France
	RegionFrance Region = "FR"
	// RegionIndia  India
	RegionIndia Region = "IN"
	// RegionItaly  Italy
	RegionItaly Region = "IT"
	// RegionJapan  Japan
	RegionJapan Region = "JP"
	// RegionMexico Mexico
	RegionMexico Region = "MX"
	// RegionUK     UK
	RegionUK Region = "UK"
	// RegionUS     US
	RegionUS Region = "US"
)

var regionEndpointMap = map[Region]string{
	RegionBrazil:  "webservices.amazon.com.br",
	RegionCanada:  "webservices.amazon.ca",
	RegionChina:   "webservices.amazon.cn",
	RegionGermany: "webservices.amazon.de",
	RegionSpain:   "webservices.amazon.es",
	RegionFrance:  "webservices.amazon.fr",
	RegionIndia:   "webservices.amazon.in",
	RegionItaly:   "webservices.amazon.it",
	RegionJapan:   "webservices.amazon.co.jp",
	RegionMexico:  "webservices.amazon.com.mx",
	RegionUK:      "webservices.amazon.co.uk",
	RegionUS:      "webservices.amazon.com",
}

var regionWebEndpointMap = map[Region]string{
	RegionBrazil:  "www.amazon.com.br",
	RegionCanada:  "www.amazon.ca",
	RegionChina:   "www.amazon.cn",
	RegionGermany: "www.amazon.de",
	RegionSpain:   "www.amazon.es",
	RegionFrance:  "www.amazon.fr",
	RegionIndia:   "www.amazon.in",
	RegionItaly:   "www.amazon.it",
	RegionJapan:   "www.amazon.co.jp",
	RegionMexico:  "www.amazon.com.mx",
	RegionUK:      "www.amazon.co.uk",
	RegionUS:      "www.amazon.com",
}

// -------------------------------------------------------------------------------------------------------------------------

// Endpoint returns API endpoint for region
func (region Region) Endpoint() string {
	return regionEndpointMap[region]
}

// HTTPSEndpoint returns HTTPS endpoint
func (region Region) HTTPSEndpoint() string {
	ep := region.Endpoint()
	if ep == "" {
		return ""
	}
	return "https://" + ep + "/onca/xml"
}

// HTTPEndpoint returns HTTP endpoint
func (region Region) HTTPEndpoint() string {
	ep := region.Endpoint()
	if ep == "" {
		return ""
	}
	return "http://" + ep + "/onca/xml"
}

// -------------------------------------------------------------------------------------------------------------------------

// Endpoint returns API endpoint for region
func (region Region) webEndpoint() string {
	return regionWebEndpointMap[region]
}

// HTTPSEndpoint returns HTTPS endpoint
func (region Region) HTTPSWebEndpoint() string {
	ep := region.webEndpoint()
	if ep == "" {
		return ""
	}
	return "https://" + ep + "/"
}

// HTTPEndpoint returns HTTP endpoint
func (region Region) HTTPWebEndpoint() string {
	ep := region.webEndpoint()
	if ep == "" {
		return ""
	}
	return "http://" + ep + "/"
}

// -------------------------------------------------------------------------------------------------------------------------

// IsValid returns region is valid
func (region Region) IsValid() bool {
	return region.Endpoint() != ""
}
