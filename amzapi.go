package amzapi

import (
	"fmt"
	"errors"
	"net/url"
	"time"
	"sort"
	"strings"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"net/http"
	"io/ioutil"
	"encoding/xml"
)

const (
	SERVICE string = "AWSECommerceService"
	VERSION string = "2013-08-01"

	OPERATION_ITEMLOOKUP = "ItemLookup"
	OPERATION_ITEMSEARCH = "ItemSearch"
)

// Client AWAS Client
type Client struct {
	AccessKeyID     string
	SecretAccessKey string
	AssociateTag    string
	Secure          bool
	Region
}

func New(accessKeyID string, secretAccessKey string, associateTag string, region Region) (*Client, error) {

	if len(accessKeyID) != 20 {
		return nil, errors.New("AccessKeyID is not specified")
	}
	if len(secretAccessKey) != 40 {
		return nil, errors.New("SecretAccessKey is not specified")
	}
	if associateTag == "" {
		return nil, errors.New("AssociateTag is not specified")
	}
	if region == "" {
		return nil, errors.New("Region is not specified")
	}
	if !region.IsValid() {
		return nil, fmt.Errorf("Invalid Region %v", region)
	}
	return &Client{
		AccessKeyID:     accessKeyID,
		SecretAccessKey: secretAccessKey,
		Region:          region,
		AssociateTag:    associateTag,
		Secure:          true,
	}, nil
}

func (c *Client) Do(operation string, params map[string]string) ([]byte, error) {

	v := url.Values{}
	v.Set("Service", SERVICE)
	v.Set("AWSAccessKeyId", c.AccessKeyID)
	v.Set("Version", VERSION)
	v.Set("Operation", operation)
	v.Set("AssociateTag", c.AssociateTag)

	for k, val := range params {
		v.Set(k, val)
	}

	v.Set("Timestamp", time.Now().UTC().Format("2006-01-02T15:04:05Z"))

	queryKeys := make([]string, 0, len(v))
	for key := range v {
		queryKeys = append(queryKeys, key)
	}
	sort.Strings(queryKeys)
	queryKeysAndValues := make([]string, len(queryKeys))
	for i, key := range queryKeys {
		k := strings.Replace(url.QueryEscape(key), "+", "%20", -1)
		v := strings.Replace(url.QueryEscape(v.Get(key)), "+", "%20", -1)
		queryKeysAndValues[i] = k + "=" + v
	}

	u, _ := url.Parse("https://webservices.amazon.de/onca/xml")

	query := strings.Join(queryKeysAndValues, "&")
	msg := "GET\n" + u.Host + "\n" + u.Path + "\n" + query
	mac := hmac.New(sha256.New, []byte(c.SecretAccessKey))
	mac.Write([]byte(msg))
	signature := base64.StdEncoding.EncodeToString(mac.Sum(nil))
	v.Set("Signature", signature)

	u.RawQuery = v.Encode()

	res, err := http.Get(u.String())
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return data, err
}

func (c *Client) DoItemLookup(params map[string]string) (*ItemLookupResponse, error) {
	result, err := c.Do(OPERATION_ITEMLOOKUP, params)
	if err != nil {
		return nil, err
	}

	var responseObject ItemLookupResponse
	if err := xml.Unmarshal(result, &responseObject); err != nil {
		return nil, err
	}

	return &responseObject, nil
}

func (c *Client) DoItemSearch(params map[string]string) (*ItemSearchResponse, error) {
	result, err := c.Do(OPERATION_ITEMSEARCH, params)
	if err != nil {
		return nil, err
	}

	var responseObject ItemSearchResponse
	if err := xml.Unmarshal(result, &responseObject); err != nil {
		return nil, err
	}

	return &responseObject, nil
}
